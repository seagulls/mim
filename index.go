package mim

import (
	"reflect"
	"strings"
)

type index[T any] struct {
	IndexDefn
	Map map[interface{}][]*T
}

type IndexDefn struct {
	Field  int
	Unique bool
}

func (i *index[T]) add(t *T) error {
	v := reflect.ValueOf(t).Elem().Field(i.Field).Interface()
	if vs, ok := v.(string); ok {
		v = strings.ToLower(vs)
	}

	m, ok := i.Map[v]
	if ok && i.Unique {
		return ErrDuplicate
	}
	if !ok {
		m = make([]*T, 0, 1)
	}
	m = append(m, t)
	i.Map[v] = m

	return nil
}

func (i *index[T]) remove(t *T) {
	v := reflect.ValueOf(t).Elem().Field(i.Field).Interface()
	if vs, ok := v.(string); ok {
		v = strings.ToLower(vs)
	}

	m, ok := i.Map[v]
	if !ok {
		return
	}

	var pos int
	for pos = range m {
		if m[pos] == t {
			n := m[pos+1:]
			m = append(m[:pos], n...)
			if len(m) == 0 {
				delete(i.Map, v)
			} else {
				i.Map[v] = m
			}
			return
		}
	}
}

func (i *index[T]) get(k interface{}) ([]*T, bool) {
	if ks, ok := k.(string); ok {
		k = strings.ToLower(ks)
	}

	indexes, ok := i.Map[k]
	return indexes, ok
}
