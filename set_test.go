package mim_test

import (
	"testing"

	"gitlab.com/seagulls/mim"
	"gitlab.com/seagulls/mim/internal"

	qt "github.com/frankban/quicktest"
)

func TestUnion(t *testing.T) {
	c := qt.New(t)

	d := []*internal.Foo{
		{ID: 0, Name: "foo"},
		{ID: 1, Name: "ted"},
		{ID: 2, Name: "bob"},
		{ID: 3, Name: "ady"},
		{ID: 4, Name: "ned"},
		{ID: 5, Name: "tim"},
		{ID: 6, Name: "bill"},
		{ID: 7, Name: "john"},
		{ID: 8, Name: "ted"},
		{ID: 9, Name: "greg"},
	}

	a := []*internal.Foo{d[0], d[1], d[2], d[3], d[4], d[5]}
	b := []*internal.Foo{d[4], d[5], d[6], d[7], d[8], d[9]}

	actual := mim.Union(a, b)
	c.Assert(actual, qt.ContentEquals, d)
}
