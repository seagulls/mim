package mim

import (
	"reflect"
	"strings"
	"time"
)

const (
	TagIndex   = `index`
	TagFilter  = `filter`
	TagSep     = `,`
	TagUnique  = `unique`
	TagPrimary = `primary`
)

// MultiIndexMap is a map accessible by multiple indexed fields.  Internally
// this is represented as a map of indexes, with each index value mapped to
// the collection of matching *T.
type MultiIndexMap[T any] struct {
	pk string
	f  map[string]int
	fn map[string]comparison
	m  map[string]*index[T]
}

// New creates a new MultiIndexMap for type T.  This parses tags on the struct
// and creates indexes with the provided name.  Index names must be unique within
// the struct, duplicates will not error but will overwrite previous indexes of
// the same name.  Indexes may be marked as `unique` and `primary` with `primary`
// indexes being `unique` by definition.
func New[T any]() (*MultiIndexMap[T], error) {
	t := new(T)
	tt := reflect.TypeOf(t).Elem()

	// check that T is a struct
	if tt.Kind() != reflect.Struct {
		return nil, ErrInvalidType
	}

	idx := &MultiIndexMap[T]{
		f:  make(map[string]int),
		fn: make(map[string]comparison),
		m:  make(map[string]*index[T]),
	}

	// iterate over the struct fields and for each field, check
	// for an `index` tag.
	for i := 0; i < tt.NumField(); i++ {
		fld := tt.Field(i)

		// Only index exported fields
		if !fld.IsExported() {
			continue
		}

		var tag string
		var fok, iok bool

		// Flag for filtering
		tag, fok = fld.Tag.Lookup(TagFilter)
		if fok {
			idx.f[tag] = i
		}

		// Skip no tag
		if !fok {
			tag, iok = fld.Tag.Lookup(TagIndex)
		}

		if !(fok || iok) {
			continue
		}

		// Split the tag on comma
		parts := strings.Split(tag, TagSep)

		// Record the field name index
		idx.f[parts[0]] = i

		// Record the field conversion function
		switch fld.Type.Kind() {
		case reflect.String:
			idx.fn[parts[0]] = compareString
		case reflect.Int64:
			idx.fn[parts[0]] = compareInt64
		case reflect.Float64:
			idx.fn[parts[0]] = compareFloat64
		case reflect.Struct:
			switch fld.Type {
			case reflect.TypeOf(time.Time{}):
				idx.fn[parts[0]] = compareTime
			}
		}

		// Only continue past here if we have an index
		if !iok {
			continue
		}

		// Create the index object
		index := &index[T]{
			IndexDefn: IndexDefn{
				Field: i,
			},
			Map: make(map[interface{}][]*T),
		}

		// iterate over the latter tag parts if there are any
		for _, atr := range parts[1:] {
			switch atr {
			case TagUnique:
				index.Unique = true
			case TagPrimary:
				if len(idx.pk) > 0 {
					return nil, ErrDuplicatePK
				}
				index.Unique = true
				idx.pk = parts[0]
			}
		}

		// store index under the index name
		idx.m[parts[0]] = index
	}

	return idx, nil
}

// Indices returns a map of index definitions.
func (m *MultiIndexMap[T]) Indices() map[string]IndexDefn {
	idx := make(map[string]IndexDefn)
	for k, v := range m.m {
		idx[k] = v.IndexDefn
	}
	return idx
}

func (m *MultiIndexMap[T]) Iterator(field string) (*Iterator[T], bool) {
	idx, ok := m.m[field]
	if !ok {
		return nil, false
	}

	return newIterator[T](idx), true
}

// Add a *T to the collection, adding to all indexes.  If any of the
// indexing operations fails the *T is removed from all indexes.
func (m *MultiIndexMap[T]) Add(t *T) (err error) {
	defer func() {
		// if we failed to add t to one of the indexes,
		// we need to make sure we remove it from all.
		if err != nil {
			m.Remove(t)
		}
	}()

	for _, idx := range m.m {
		if err = idx.add(t); err != nil {
			return err
		}
	}

	return nil
}

// Remove *T from all indexes.
func (m *MultiIndexMap[T]) Remove(t *T) {
	for _, idx := range m.m {
		idx.remove(t)
	}
}

// Get all *T from the listed indices.  If no index name is provided
// then the default index is used.
func (m *MultiIndexMap[T]) Get(k interface{}, indices ...string) ([]*T, bool) {
	// use default pk if no indices provided.
	if len(indices) == 0 {
		// if there is no default then fail.
		if len(m.pk) == 0 {
			return nil, false
		}

		mp, ok := m.m[m.pk]
		if !ok {
			return nil, false
		}
		return mp.get(k)
	}

	var out []*T
	// iterate over the indices and return the union of sets returned.
	for _, index := range indices {
		mp, ok := m.m[index]
		if !ok {
			continue
		}
		set, ok := mp.get(k)
		if !ok {
			continue
		}
		out = Union(out, set)
	}

	return out, len(out) > 0
}

// Search all *T from the listed terms.  If no index name is provided
// then the default index is used.  The terms are used to grab the respective
// sets, then the smallest set is filtered by all search terms.
func (m *MultiIndexMap[T]) Search(terms map[string]interface{}) ([]*T, bool) {
	// use default pk if no terms provided.
	if len(terms) == 0 {
		return nil, false
	}

	var out []*T
	var set []*T
	// iterate over the terms and determine smallest matching set
	for k, v := range terms {
		// Skip this value if it is a Range function.
		if _, ok := v.(Comparison); ok {
			continue
		}

		mp, ok := m.m[k]
		if !ok {
			continue
		}
		ds, ok := mp.get(v)
		if !ok {
			return nil, false
		}
		if len(ds) == 0 {
			return nil, false
		}
		if len(ds) < len(set) || set == nil {
			set = ds
		}
	}

	// filter using all terms
outer:
	for _, t := range set {
		for k, v := range terms {
			index, ok := m.f[k]
			if !ok {
				continue outer
			}

			// get the field value
			it := reflect.ValueOf(t).Elem().FieldByIndex([]int{index}).Interface()

			// check if v is a Range function, if so process it
			if rfn, ok := v.(Comparison); ok {
				if !rfn(it) {
					continue outer
				}
				// check the next term
				continue
			}

			// get the compare function for the field
			fn, ok := m.fn[k]
			if !ok {
				continue outer
			}

			// run the predefined compare function
			if !fn(v, it) {
				continue outer
			}
		}
		out = append(out, t)
	}

	return out, len(out) > 0
}

type Comparison func(interface{}) bool
type comparison func(expect, actual interface{}) bool

var (
	compareString = func(expect, actual interface{}) bool {
		ve, ok := expect.(string)
		if !ok {
			return false
		}
		va, ok := actual.(string)
		if !ok {
			return false
		}
		return strings.EqualFold(ve, va)
	}

	compareInt64 = func(expect, actual interface{}) bool {
		ve, ok := expect.(int64)
		if !ok {
			return false
		}
		va, ok := actual.(int64)
		if !ok {
			return false
		}
		return ve == va
	}

	compareFloat64 = func(expect, actual interface{}) bool {
		ve, ok := expect.(float64)
		if !ok {
			return false
		}
		va, ok := actual.(float64)
		if !ok {
			return false
		}
		return ve == va
	}

	compareTime = func(expect, actual interface{}) bool {
		ve, ok := expect.(time.Time)
		if !ok {
			return false
		}
		va, ok := actual.(time.Time)
		if !ok {
			return false
		}
		return ve == va
	}
)
