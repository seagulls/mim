# MultiIndexMap

The MultiIndexMap is a simple implementation of a map with multiple keys.  It follows the caveats of normal Go maps in that values retrieved from the map are unordered.  The creation of the MIM, adding and removing keys make use of reflection, so this is best used with data sets which do not frequently change, but require frequent lookups as these involve a fairly straight forward map access.  Generics have been used to ensure type consistency at compile time.

## Limitations

While there is no limitation to the size or depth of the structs that may be added there are limitations on what may be indexed;

- Structs must be added to the collection as pointers
- Indexes may be on pointer values, but expect unusual results unless you can ensure that equal underlying values are represented by pointers to the same location as the value that is actually used in the index will be the pointer and not the underlying value.

## Example Use

```go
type Geo struct {
    ID       int           `index:"id,primary"`
    Name     string        `index:"name,unique"`
    Lat      float64       `index:"lat"`
    Lon      float64       `index:"lon"`
    Town     string        `index:"town"`
    County   string        `index:"county"`
    Country  string        `index:"country"`
    PostCode string        `index:"postcode"`
    TZOffset time.Duration `index:"tz_offset"`
}
```

The `Geo` struct will have all fields indexed, with both the `ID` and `Name` fields being `unique`.  Get() operations without specified index name(s) will use the `id` index by default.

## Errors

All errors returned by MIM are constants of type `mim.MultiIndexMapError`.  Possible errors are;

- `ErrDuplicate` is returned when an object is added with a duplicate unique value.
- `ErrDuplicatePK` is returned when a struct has multiple fields defined as `primary`
- `ErrInvalidType` is returned when an index is created on a non-struct type.  You really should not be seeing this one.
