package mim

type Iterator[T any] struct {
	data *index[T]
	pi   int
	si   int
}

func newIterator[T any](data *index[T]) *Iterator[T] {
	return &Iterator[T]{
		data: data,
		pi:   -1,
		si:   -1,
	}
}

func (i *Iterator[T]) Next() (*T, bool) {
	if i.pi == -1 {
		i.pi = 0
	}
	if i.si == -1 {
		i.si = 0
	}

	if i.pi >= len(i.data.Map) {
		return nil, false
	}

	if i.si >= len(i.data.Map[i.pi]) {
		i.pi++
		i.si = 0
		return i.Next()
	}

	t := i.data.Map[i.pi][i.si]
	i.si++
	return t, true
}
