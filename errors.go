package mim

type MultiIndexMapError string

func (err MultiIndexMapError) Error() string {
	return string(err)
}

const (
	ErrDuplicate   = MultiIndexMapError(`duplicate value`)
	ErrDuplicatePK = MultiIndexMapError(`duplicate primary key`)
	ErrInvalidType = MultiIndexMapError(`not a struct pointer`)
)
