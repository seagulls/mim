package mim

func Union[T comparable](a []T, b []T) []T {
	u := make(map[T]struct{})
	for _, av := range a {
		u[av] = struct{}{}
	}
	for _, bv := range b {
		u[bv] = struct{}{}
	}
	out := make([]T, 0, len(u))
	for uv := range u {
		out = append(out, uv)
	}
	return out
}
