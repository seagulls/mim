package mim_test

import (
	"testing"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/mim"
	"gitlab.com/seagulls/mim/internal"
)

func TestMultiIndexMap(t *testing.T) {
	c := qt.New(t)

	idx, err := mim.New[internal.Foo]()
	c.Assert(err, qt.IsNil)

	{
		expect := map[string]mim.IndexDefn{
			`idx_id`:   {Field: 0, Unique: true},
			`idx_type`: {Field: 1, Unique: false},
			`idx_name`: {Field: 3, Unique: false},
		}
		c.Assert(idx.Indices(), qt.ContentEquals, expect)
	}

	data := []*internal.Foo{
		{ID: 0, Type: "user", Sub: "a", Name: "foo"},
		{ID: 1, Type: "user", Sub: "b", Name: "ted"},
		{ID: 2, Type: "user", Sub: "a", Name: "bob"},
		{ID: 3, Type: "user", Sub: "b", Name: "ady"},
		{ID: 4, Type: "user", Sub: "a", Name: "ned"},
		{ID: 5, Type: "user", Sub: "b", Name: "tim"},
		{ID: 6, Type: "user", Sub: "a", Name: "bill"},
		{ID: 7, Type: "user", Sub: "b", Name: "john"},
		{ID: 8, Type: "user", Sub: "a", Name: "ted"},
		{ID: 9, Type: "user", Sub: "b", Name: "greg"},
	}

	for _, obj := range data {
		err = idx.Add(obj)
		c.Assert(err, qt.IsNil)
	}

	{
		actual, ok := idx.Get(4)
		c.Assert(ok, qt.IsTrue)
		c.Assert(actual, qt.ContentEquals, []*internal.Foo{data[4]})

		actual, ok = idx.Get(`ned`, `idx_name`)
		c.Assert(ok, qt.IsTrue)
		c.Assert(actual, qt.ContentEquals, []*internal.Foo{data[4]})
	}

	idx.Remove(data[4])
	{
		_, ok := idx.Get(4)
		c.Assert(ok, qt.IsFalse)

		_, ok = idx.Get(`ned`, `idx_name`)
		c.Assert(ok, qt.IsFalse)
	}

	{
		actual, ok := idx.Get(8)
		c.Assert(ok, qt.IsTrue)
		c.Assert(actual, qt.ContentEquals, []*internal.Foo{data[8]})

		actual, ok = idx.Get(`ted`, `idx_name`)
		c.Assert(ok, qt.IsTrue)
		c.Assert(actual, qt.ContentEquals, append([]*internal.Foo{data[1]}, data[8]))
	}

	idx.Remove(data[8])
	{
		actual, ok := idx.Get(8)
		c.Assert(ok, qt.IsFalse)
		c.Assert(actual, qt.HasLen, 0)

		actual, ok = idx.Get(`ted`, `idx_name`)
		c.Assert(ok, qt.IsTrue)
		c.Assert(actual, qt.ContentEquals, []*internal.Foo{data[1]})
	}

	{
		dup := &internal.Foo{ID: 5, Name: `billy bongo`}
		err = idx.Add(dup)
		c.Assert(err, qt.Not(qt.IsNil))

		actual, ok := idx.Get(`billy bongo`, `idx_name`)
		c.Assert(ok, qt.IsFalse)
		c.Assert(actual, qt.HasLen, 0)
	}
}

func TestMultiIndexMap_Search(t *testing.T) {
	c := qt.New(t)

	idx, err := mim.New[internal.Foo]()
	c.Assert(err, qt.IsNil)

	data := []*internal.Foo{
		{ID: 0, Type: "user", Sub: "a", Name: "foo"},
		{ID: 1, Type: "user", Sub: "b", Name: "ted"},
		{ID: 2, Type: "user", Sub: "a", Name: "bob"},
		{ID: 3, Type: "user", Sub: "b", Name: "ady"},
		{ID: 4, Type: "user", Sub: "a", Name: "ned"},
		{ID: 5, Type: "user", Sub: "b", Name: "tim"},
		{ID: 6, Type: "user", Sub: "a", Name: "bill"},
		{ID: 7, Type: "user", Sub: "b", Name: "john"},
		{ID: 8, Type: "user", Sub: "a", Name: "ted"},
		{ID: 9, Type: "user", Sub: "b", Name: "greg"},
	}

	for _, obj := range data {
		err = idx.Add(obj)
		c.Assert(err, qt.IsNil)
	}

	terms := map[string]interface{}{
		`idx_name`: `ted`,
	}

	expect := []*internal.Foo{
		data[1],
		data[8],
	}

	actual, ok := idx.Search(terms)
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.ContentEquals, expect)

	terms = map[string]interface{}{
		`idx_name`: `ted`,
		`f_sub`:    `a`,
	}

	expect = []*internal.Foo{
		data[8],
	}

	actual, ok = idx.Search(terms)
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.ContentEquals, expect)

	terms = map[string]interface{}{
		`idx_name`: `ted`,
		`f_sub`:    `b`,
	}

	expect = []*internal.Foo{
		data[1],
	}

	actual, ok = idx.Search(terms)
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.ContentEquals, expect)

	terms = map[string]interface{}{
		`idx_type`: `user`,
		`idx_name`: mim.Comparison(func(arg interface{}) bool {
			av, ok := arg.(string)
			if !ok {
				return false
			}

			return "" < av && av < "e"
		}),
	}

	expect = []*internal.Foo{
		data[2],
		data[3],
		data[6],
	}

	actual, ok = idx.Search(terms)
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.ContentEquals, expect)
}

func TestMultiIndexMapIterator(t *testing.T) {
	c := qt.New(t)

	idx, err := mim.New[internal.Foo]()
	c.Assert(err, qt.IsNil)

	{
		expect := map[string]mim.IndexDefn{
			`idx_id`:   {Field: 0, Unique: true},
			`idx_type`: {Field: 1, Unique: false},
			`idx_name`: {Field: 3, Unique: false},
		}
		c.Assert(idx.Indices(), qt.ContentEquals, expect)
	}

	data := []*internal.Foo{
		{ID: 0, Type: "user", Sub: "a", Name: "foo"},
		{ID: 1, Type: "user", Sub: "b", Name: "ted"},
		{ID: 2, Type: "user", Sub: "a", Name: "bob"},
		{ID: 3, Type: "user", Sub: "b", Name: "ady"},
		{ID: 4, Type: "user", Sub: "a", Name: "ned"},
		{ID: 5, Type: "user", Sub: "b", Name: "tim"},
		{ID: 6, Type: "user", Sub: "a", Name: "bill"},
		{ID: 7, Type: "user", Sub: "b", Name: "john"},
		{ID: 8, Type: "user", Sub: "a", Name: "ted"},
		{ID: 9, Type: "user", Sub: "b", Name: "greg"},
	}

	for _, obj := range data {
		err = idx.Add(obj)
		c.Assert(err, qt.IsNil)
	}

	it, ok := idx.Iterator(`idx_id`)
	c.Assert(ok, qt.IsTrue)

	expect := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	actual := make([]int, 0, len(data))
	for {
		v, ok := it.Next()
		if !ok {
			break
		}
		actual = append(actual, v.ID)
	}

	c.Assert(actual, qt.ContentEquals, expect)
}
