package internal

type Foo struct {
	ID   int    `index:"idx_id,primary"`
	Type string `index:"idx_type"`
	Sub  string `filter:"f_sub"`
	Name string `index:"idx_name"`
}
